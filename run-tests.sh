#!/bin/bash 

let SKIP_TO=0
let IDX=0

for filename in test-case/input-split/*; do # https://stackoverflow.com/questions/20796200/how-to-iterate-over-files-in-a-directory-with-bash
    basename "$filename"; 
    let IDX++
    if [ $IDX -lt $SKIP_TO ]
        then
            continue 
    fi
    krun -o none "$filename" | diff -w - "test-case/reference-split/$(basename $filename)"
done

