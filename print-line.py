#!/usr/bin/python3
from sys import argv
assert len(argv) == 2
with open(argv[1], "r") as f:
    lines = f.readlines()
    while True:
        try:
            print(lines[int(input())-1], end='')
        except EOFError:
            break
