#!/usr/bin/python3

import instructions
from used_opcodes import *
from disasm_to_iclass import *

print("//                                        Prefix (REPVAL), Disasm, Suffix")
print("    syntax Opcode ::= DisassemblyToOpcode(Int, String, String) [function]") 

PREFIX_FORMS = [
    [(0, "")],
    [(2, "rep ")],
    [(2, "repz "), (3, "repnz ")] 
]


emitted = set()
for inst in instructions.instruction_list:
    opcode = inst.iname 
    for suffix in inst.suffixes:        
        prefix_type = min(opcode_prefix_type.get(opcode, 0), 2)
        for (repval, prefix) in PREFIX_FORMS[prefix_type]:
            key = "&".join([str(prefix), opcode, suffix])
            if not key in emitted:
                print("    rule DisassemblyToOpcode({0}, \"{1}\", \"{2}\") => {3}{1}{2}".format(repval, opcode, suffix, prefix))
                emitted.add(key)
