#!/usr/bin/python3

import fileinput

rewrites = {
    "al": "%al",
    "ax": "%ax",
    "cl": "%cl",
    "eax": "%eax",
    "imm16": "$0x7FFF",
    "imm32": "$0x7FFFFFFF",
    "imm64": "$0x7FFFFFFFFFFFFFFF",
    "imm8" : "$0x7F",
    "m128": "(%rbx)",
    "m16": "(%rbx)",
    "m256": "(%rbx)",
    "m32": "(%rbx)",
    "m64": "(%rbx)",
    "m8": "(%rbx)",
    "one": "$1",
    "r16": "%bx",
    "r32": "%ebx",
    "r64": "%rbx",
    "r8": "%bl",
    "rax": "%rax",
    "rh": "%bh",
    "xmm": "%xmm1",
    "xmm0": "%xmm0",
    "ymm": "%ymm1"
}

for line in fileinput.input():
    parts = line.split()
    opcodes = [elem for elem in parts if elem not in rewrites]
    operands = [rewrites[elem] for elem in parts if elem in rewrites]
    operands.reverse()
    print(" ".join(opcodes) + " " + ", ".join(operands))

