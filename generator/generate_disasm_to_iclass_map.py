#!/usr/bin/python3

# 
# A Re-Implementation of Intels disassembler from XED in K semantics.
#

import instructions

result = {}
for inst in instructions.instruction_list:
    for possibility in [inst.iname + suffix for suffix in inst.suffixes]:
        existing = result.get(possibility, set())
        existing.add(inst.iclass)
        result[possibility] = existing

print("lookup_table = {")
for disasm in result:
    line = "    \"{0}\": [{1}],"
    iclasses = ", ".join(["\"{0}\"".format(iclass) for iclass in list(result[disasm])])
    print(line.format(disasm, iclasses))
print("}")

footer = """
def get_iclasses_from_disasms(disasms):
    return list(set([lookup_table[disasm] for disasm in disasms]))

if __name__ == "__main__":
    for ret in get_iclasses_from_disasms(sys.argv[1:]):
        print(ret)
"""
print(footer)
