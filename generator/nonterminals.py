import re
from macros import macro_map

nonterminal_decl_pattern = re.compile("\s*(?:[a-z_]*\s+)?(\w+)\\(\\)::")
def parse_nonterminal_token(line):
    match = nonterminal_decl_pattern.match(line) # Lots of duplicate code here... should figure out a smarter way to do this.  Maybe Lambdas?
    return match.group(1) if match is not None else None

list_type = type([])
def flatten(lst):
    ret = []
    for element in lst:
        if type(element) == list_type:
            ret.extend(element)
        else:
            ret.append(element)
    return ret

def split_list_by_class(lst):
    result_dict = {}
    for element in lst:
        class_name = element.__class__.__name__
        if class_name in result_dict:
            result_dict[class_name].append(element)
        else:
            result_dict[class_name] = [element]
    return result_dict

class Equation(object):
    def __init__(self, name, val, is_equal):
        self.name = name.replace("_", "")
        self.val = val
        self.is_equal = is_equal == "="

class BitVar(object):
    def __init__(self, name, length, required_val = None):
        self.name = name.replace("_", "")
        self.length = length
        self.required_val = required_val

class Capture(object):
    def __init__(self, name, vars_):
        self.name = name.replace("_", "")
        self.vars = vars_

class NoncapturingMatch(object):
    def __init__(self, pattern):
        self.pattern = pattern

class OtherwiseCondition(object):
    def __init__(self):
        pass
otherwise_singleton = OtherwiseCondition()

class NonterminalCondition(object):
    def __init__(self, name):
        self.name = name

antecedent_classes = [Equation.__name__, Capture.__name__, NoncapturingMatch.__name__, OtherwiseCondition.__name__]

numeric_pattern = re.compile("^\d+$")
class SimpleAssignment(object):
    def __init__(self, name, val):
        self.name = name.replace("_", "")
        if numeric_pattern.match(val) != None:
            val = int(val)
            self.sort = "Number"
        else:
            self.sort = "Register"
        self.val = val

class ComplexAssignment(object):
    def __init__(self, name, func_name):
        self.name = name.replace("_", "")
        self.func_name = func_name

class DecoderError(object):
    def __init__(self):
        pass

error_singleton = DecoderError()

class IgnoredLiteral(object):
    def __init__(self, literal):
        self.literal = literal

consequent_classes = [SimpleAssignment.__name__, ComplexAssignment.__name__, DecoderError.__name__, IgnoredLiteral.__name__]

class NonterminalAlternative(object):
    def __init__(self, name, antecedent, consequent):
        self.name = name
        self.antecedent = antecedent
        self.consequent = consequent

def parse_single_element(element, possibilities, fatal=True):
    if element in macro_map:
        return [parse_single_element(e, possibilities) for e in macro_map[element]]
    for possibility in possibilities:
        ret = possibility(element)
        if ret is not None:
            return ret
    if fatal:
        print("Failed to match anything")
        raise RuntimeError(element) 
    return None

hex_equation_pattern = re.compile("^(\w+)([!]?=)0x([0-9a-fA-F]+)$")
def hex_equation(element):
    match = hex_equation_pattern.match(element)
    return Equation(match.group(1), int(match.group(3), 16), match.group(2)) if match is not None else None

bit_equation_pattern = re.compile("^(\w+)([!]?=)0b([01]+)$")
def bit_equation(element):
    match = bit_equation_pattern.match(element)
    return Equation(match.group(1), int(match.group(3), 2), match.group(2)) if match is not None else None

normal_equation_pattern = re.compile("^(\w+)([!]?=)([0-9]+)$")
def normal_equation(element):
    match = normal_equation_pattern.match(element)
    return Equation(match.group(1), int(match.group(3)), match.group(2)) if match is not None else None

equation_types = [hex_equation, bit_equation, normal_equation]
def parse_equation(element):
    return parse_single_element(element, equation_types, False)


capture_pattern = re.compile("^([A-Z0-9]+)\\[([a-z_]+)\\]$")
def parse_capture(element):
    match = capture_pattern.match(element)
    if match is None:
        return None
    return Capture(match.group(1), [BitVar(s, len(s)) for s in match.group(2).split("_")])

literal_capture_pattern = re.compile("^([A-Z0-9]+)\\[0b([01]+)\\]$")
def parse_literal_capture(element):
    match = literal_capture_pattern.match(element)
    return Capture(match.group(1), [BitVar("", len(match.group(2)), int(match.group(2), 2))]) if match is not None else None

length_capture_pattern = re.compile("^([A-Z0-9]+)\\[[a-z]/(\d+)\\]$")
def parse_length_capture(element):
    match = length_capture_pattern.match(element)
    return Capture(match.group(1), [BitVar("", int(match.group(2)))]) if match is not None else None

literal_match_pattern = re.compile("^(0x[A-Fa-f0-9]+|0b[01_]+)$")
def parse_noncapturing_match(element):
    match = literal_match_pattern.match(element)
    return NoncapturingMatch(match.group(1)) if match is not None else None

nonterminal_pattern = re.compile("^(\w+)\\(\\)[:]?$")
def parse_nonterminal_pattern(element):
    match = nonterminal_pattern.match(element)
    return NonterminalCondition(match.group(1)) if match is not None else None

def parse_otherwise(element):
    return otherwise_singleton if element == "otherwise" else None

# Conditional can take the form of an EQUATION, CAPTURE, LITERALCAPTURE, or NONTERMINAL
# Of these, only an EQUATION or LITERALCAPTURE can actually fail, I think (CAPTURE/NONTERMINAL maybe if we're out of bits, but that should be impossible).
condition_types = [parse_equation, parse_capture, parse_literal_capture, parse_length_capture, parse_noncapturing_match, parse_nonterminal_pattern, parse_otherwise]
def parse_condition(element):
    return parse_single_element(element, condition_types)

def parse_antecedent(line):
    return split_list_by_class(flatten([parse_condition(element) for element in line.split()]))

simple_assign_pattern = re.compile("^(\w+)=([A-Z0-9_]+)$")
def parse_simple_assign(element):
    match = simple_assign_pattern.match(element)
    return SimpleAssignment(match.group(1), match.group(2)) if match is not None else None

complex_assign_pattern = re.compile("^(\w+)=(\w+)\\(\\)[:]?$") # What's the colon at the end for?
def parse_complex_assign(element):
    match = complex_assign_pattern.match(element)
    return ComplexAssignment(match.group(1), match.group(2)) if match is not None else None

def parse_error(element):
    return error_singleton if element == "error" else None

ignored_literal_pattern = re.compile("^(XED_RESET|enc|\w+=\w+)$")
def parse_ignored_literal(element):
    match = ignored_literal_pattern.match(element)
    return IgnoredLiteral(match.group(1)) if match is not None else None 

result_types = [parse_simple_assign, parse_complex_assign, parse_error, parse_ignored_literal]
def parse_result(element):
    return parse_single_element(element, result_types)

def parse_consequent(line):
    return split_list_by_class(flatten([parse_result(element) for element in line.split()]))

alternative_pattern = re.compile("\s*([^#|]+)\\|([^#]*)\n")
def parse_alternative(name, line):
    match = alternative_pattern.match(line)
    if match is None:
        return None
    antecedent = parse_antecedent(match.group(1))
    consequent = parse_consequent(match.group(2))
    return NonterminalAlternative(name, antecedent, consequent)

def parse_nonterminals(lines):
    nonterminals = []
    token = None
    alternatives = None
    for line in lines:
        token_on_line = parse_nonterminal_token(line)
        if token_on_line:
            old_token = token
            old_alternatives = alternatives
            token = token_on_line
            alternatives = []
            if old_token is None:
                continue
            nonterminals.append((old_token, old_alternatives))
        else:
            if alternatives is not None:
                parsed_alternative = parse_alternative(token, line)
                if parsed_alternative is not None:
                    alternatives.append(parsed_alternative)
    nonterminals.append((token, alternatives))
    return dict(nonterminals)

nonterminals_map = None
with open("datafiles/all-dec-patterns.txt", "r") as f:
    nonterminals_map = parse_nonterminals(f.readlines())
