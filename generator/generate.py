#!/usr/bin/python3

#BEGIN_LEGAL 
# 
# A Re-Implementation of Intels disassembler from XED in K semantics.
#
# Copyright (c) 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#                         
# END_LEGAL

import instructions
import nonterminals
import sys
from kcodegen import kRewrite

class FreshVariableGenerator(object):
    def __init__(self, prefix="I"):
        self._prefix = prefix
        self._val = 0
        self.next_variable()

    def next_variable(self):
        self._current = self._prefix + str(self._val)
        self._val += 1
        return self._current

    def last_variable(self):
        return self._current

def gen_from_nonterminal(nonterm, additional = ""):
    rewrite = kRewrite()
    fresh_vars = FreshVariableGenerator()

    pattern = nonterm.antecedent
    assignments = nonterm.consequent

    literal = " ".join([str(int(match.pattern, 16)) for match in pattern.get(nonterminals.NoncapturingMatch.__name__, [])]) 
    if len(literal) > 0:
        rewrite.add_literal_match("dynamicDecoderBuffer", literal + " _:Ints ")

    reads = [eq for eq in pattern.get(nonterminals.Equation.__name__, [])]
    writes = [assign for assign in assignments.get(nonterminals.SimpleAssignment.__name__, [])]

    bound_names = {}
    matches = []
    for read in reads:
        for write in writes:
            if read.name == write.name:
                rewrite.add_rewrite(read.name, read.val, write.val)
                bound_names[read.name] = read.val

    for cap in pattern.get(nonterminals.Capture.__name__, []):
        assert len(cap.vars) > 0
        name = cap.name
        value = cap.vars[0].required_val
        if value is not None:
            bound_names[name] = value
            rewrite.add_literal_match(name, value)

    inequalities = []
    for eq in pattern.get(nonterminals.Equation.__name__, []):
        if eq.name in bound_names:
            assert (bound_names[eq.name] == eq.val) == eq.is_equal # Either both equal, or both not equal.
        else:
            if eq.is_equal:
                rewrite.add_literal_match(eq.name, eq.val)
            else:
                rewrite.add_variable(eq.name, fresh_vars.next_variable(), "Int")
                inequalities.append((fresh_vars.last_variable(), eq.val))
    if len(inequalities) > 0:
        rewrite.set_condition(" andBool ".join(["{0} =/=Int {1}".format(name, value) for (name, value) in inequalities]))


    if len(pattern.get(nonterminals.OtherwiseCondition.__name__, [])) > 0:
        rewrite.add_attribute("owise")


    for assign in assignments.get(nonterminals.SimpleAssignment.__name__, []):
        if assign.name not in bound_names:
            rewrite.add_rewrite(assign.name, "_", assign.val)

    decode_nonterminals = " ~> ".join([nonterm.name for nonterm in pattern.get(nonterminals.NonterminalCondition.__name__, [])])

    if len(additional) > 0:
        decode_nonterminals += (" ~> " if len(decode_nonterminals) > 0 else "") + additional

    for assign in assignments.get(nonterminals.ComplexAssignment.__name__, []):
        decode_nonterminals += (" ~> " if len(decode_nonterminals) > 0 else "") + assign.func_name + (" ~> OUTREGTo" + assign.name if assign.name != "OUTREG" else "")

    if len(assignments.get(nonterminals.DecoderError.__name__, [])) > 0:
        rewrite.add_rewrite("k", nonterm.name, "DecoderError", False)
    else:
        rewrite.add_rewrite("k", nonterm.name, decode_nonterminals if len(decode_nonterminals) else ".", False)

    return rewrite

def gen_from_inst(inst): 
    ret = gen_from_nonterminal(inst, "ScanForDisp ~> ScanForImmediate")
    ret.add_rewrite("ICLASS", "_", inst.iclass)
    ret.add_rewrite("INUM", "_", inst.id)
    ret.add_rewrite("CATEGORY", "_", "\"CATEGORY_{0}\"".format(inst.category))
    ret.add_rewrite("INAME", "_", "\"" + inst.iname + "\"")
    ret.add_rewrite("ATTRIBUTES", "_", "{0}".format(" ".join(["ListItem(A_{0})".format(name) for name in inst.attributes])) if len(inst.attributes) > 0 else ".List")
    ret.add_rewrite("OPERANDS", "_", " ".join(["ListItem(OperandMetadata({0},{1},{2},{3},{4},{5},{6}))".format(name, str(mask).lower(), vis, sort, width16, width32, width64) for (_, name, mask, vis, sort, width16, width32, width64, _) in inst.operands]) if len(inst.operands) > 0 else ".List")
    if not inst.has_vexvalid:
        ret.add_literal_match("VEXVALID", "0")
    return ret

def print_outreg_to(name):
    print("syntax K ::= \"OUTREGTo" + name + "\"")
    print("rule <k> OUTREGTo" + name + " => . ... </k>")
    print("<OUTREG> M </OUTREG>")
    print("<" + name + "> _ => M </" + name + ">")


print("require \"decoder-configuration.k\"")
print("module GENERATED-DECODER-RULES")
print("imports DOMAINS")
print("imports COLLECTIONS")
#print("imports DECODER-INTERFACE")
print("imports DECODER-CONFIGURATION")
print("syntax K ::= \"DecoderError\"")
print("syntax K ::= \"ScanForDisp\"")
print("syntax K ::= \"ScanForImmediate\"")
print('syntax OperandVisibility ::= "SUPPRESSED" | "IMPLICIT" | "EXPLICIT"')
print('syntax OperandWidth ::= Int | "AUTO" | "VECTOR" | "SSZ" | "ASZ"') # Explicit bit count, AUTO for register arguments whose width is inferred from its value, and VECTOR for NELEM*ELEMSIZE
print('syntax KResult ::= OperandWidth')
print("syntax KItem ::= OperandMetadata(KItem, Bool, OperandVisibility, KItem, OperandWidth, OperandWidth, OperandWidth)") # Name, IsWriteMask, Visibility, C-Type, Wdith16, Width32, Width64

outregs_done = set()
for name in nonterminals.nonterminals_map:
    for alternative in nonterminals.nonterminals_map[name]:
        for assign in alternative.consequent.get(nonterminals.ComplexAssignment.__name__, []):
            if assign.name != "OUTREG" and assign.name not in outregs_done:
                print_outreg_to(assign.name)
                outregs_done.add(assign.name)

attributes_done = set()
operand_metadata_ids = set()
operand_sort_set = set()
for inst in instructions.instruction_list:
    for operand in inst.operands:
        assign = operand[0]
        if assign.__class__.__name__ == "ComplexAssignment" and assign.name not in outregs_done:
            print_outreg_to(assign.name)
            outregs_done.add(assign.name)
    attributes_done |= set(inst.attributes)
    operand_metadata_ids |= set([id_ for (_, id_, _, _, _, _, _, _, _) in inst.operands])
    operand_sort_set |= set([sort for (_, _, _, _, sort, _, _, _, _) in inst.operands])

print("syntax KItem ::= " + " | ".join(['"A_{0}"'.format(attribute) for attribute in attributes_done]))
print("syntax KItem ::= " + " | ".join(['"{0}"'.format(id_) for id_ in operand_metadata_ids]))
print("syntax KItem ::= " + " | ".join(['"{0}"'.format(sort) for sort in operand_sort_set]))


print("syntax K ::= \"DynamicDecodeInstruction\"")
iclasses = list(set(["\"{0}\"".format(inst.iclass) for inst in instructions.instruction_list]))
print("syntax IClass ::= " + " | ".join(iclasses))

desired = set([n.upper() for n in sys.argv[1:]])

for inst in instructions.instruction_list:
    if len(desired) == 0 or inst.iclass in desired:
        print("// UNAME: " + inst.uname)
        print(gen_from_inst(inst).generate_rule().replace("XED_", ""))

for name in nonterminals.nonterminals_map:
    if "SPLITTER" in name or "PREFIX" in name:
        continue
    print("syntax K ::= \"" + name + "\"")
    for alternative in nonterminals.nonterminals_map[name]:
        print(gen_from_nonterminal(alternative).generate_rule().replace("XED_", ""))

print("endmodule")
