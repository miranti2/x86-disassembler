import re
from copy import copy
import macros
import nonterminals
from operand_sorts import *
from operand_widths import *
import sys

ends_with_backslash = re.compile("(.*)\\\\$")
def merge_escaped_newlines(lines):
    new_lines = []
    last_line = ""
    for line in lines:
        match = ends_with_backslash.match(line)
        if match is not None:
            last_line += match.group(1)
        else:
            last_line += line
            new_lines.append(last_line)
            last_line = ""
    return new_lines

def split_instructions(lines):
    start = re.compile("{")
    end = re.compile("}")
    instructions = []
    first_line = None
    for i in range(len(lines)):
        if start.match(lines[i]) is not None:
            first_line = i
        elif end.match(lines[i]) is not None:
            instructions.append(lines[(first_line+1):i])
            first_line = None
    return instructions 

def resolve_operand_macro(operand):
    if operand in macros.macro_map:
        lst = macros.macro_map[operand]
        assert len(lst) == 1
        return macros.macro_map[operand][0]
    else:
        return operand


stack_operations = {
        "XED_REG_STACKPUSH": "w",
        "XED_REG_STACKPOP": "r"
}

def parse_operand(operand, mem_idx=0):
    operand = resolve_operand_macro(operand)
    segments = operand.split(":")
    name = None
    is_mask = False
    stack_op = None
    width_name = None
    widths = ["AUTO", "AUTO", "AUTO"]
    assignment = None
    is_float = False
    for possibility in segments[2:]:
        if possibility in operand_widths:
            width_name = possibility
            (width_type, widths) = operand_widths[possibility]
            is_float = width_type == "f32" or width_type == "f64"

            if width_type == "var":
                widths = ["VECTOR", "VECTOR", "VECTOR"]
            elif len(widths) == 1:
                widths = 3 * widths
            break
    if "=" in segments[0]:
        split = segments[0].split("=")
        name = split[0]
        is_mask = split[1] == "MASK1()"
        
        if split[1] in stack_operations:
            #print("MEM{0}:{2}:{1}:SUPP".format(mem_idx, width_name, stack_operations[split[1]]), file=sys.stderr)
            return [parse_operand("MEM{0}:{2}:{1}:SUPP".format(mem_idx, width_name, stack_operations[split[1]]))[0],
                    parse_operand("BASE{0}=SrSP():rw:SUPP".format(mem_idx))[0],
                    parse_operand("SEG{0}=FINAL_SSEG{0}():r:SUPP".format(mem_idx))[0]]

        if "(" in segments[0]: # Complex
            assignment = nonterminals.ComplexAssignment(split[0], split[1][:split[1].find("(")])
        else:
            assignment = nonterminals.SimpleAssignment(split[0], split[1])

    else:
        assignment = nonterminals.SimpleAssignment(segments[0], "1")
        name = segments[0]
    visibility = None
    if len(segments) >= 4:
        if segments[3] in operand_visibilities:
            visibility = operand_visibilities[segments[3]]
        elif segments[2] in operand_visibilities:
            visibility = operand_visibilities[segments[2]]
        else:
            visibility = "EXPLICIT"
    elif len(segments) == 3:
        if segments[2] in operand_visibilities:
            visibility = operand_visibilities[segments[2]]
        else:
            visibility = "EXPLICIT"
    else:
        visibility = "EXPLICIT"
    if visibility == "ECOND":
        return []
    else:
        return [(assignment, "OPERAND_" + name, is_mask, visibility, operand_sorts[name], widths[0], widths[1], widths[2], is_float)]


def parse_operands(arg):
    mem_count = 0
    ret = []
    for op in arg.split():
        parsed = parse_operand(op, mem_count)
        if len(parsed) >= 1 and "MEM" in parsed[0][1]:
            assert mem_count < 2
            mem_count += 1
        ret.extend(parsed)
    return ret

operand_actions = ["r", "w", "crw", "rw", "rcw"]
operand_visibilities = {
        "SUPP": "SUPPRESSED",
        "IMPL": "IMPLICIT",
        "EXPL": "EXPLICIT",
        "DEFAULT": "EXPLICIT", # Assuming EXPL is equivalent to default.  Can't find where in the code XED does this but the final enum just doesn't have a DEFAULT member.
        "SUPPRESSED": "SUPPRESSED",
        "IMPLICIT": "IMPLICIT",
        "EXPLICIT": "EXPLICIT",
        "ECOND": "ECOND"
}

suffixes = {
    "8": "b",
    "16": "w",
    "32": "l",
    "64": "q",
    "128": "x",
    "256": "y",
    "512": "z",

#    "0": "",
#    "48": "",
#    "80": "",
#    "96": "",
#    "112": "",
#    "160": "",
#    "224": "",
#    "320": "",
#    "752": "",
#    "864": "",
#    "4096": "",
#    "4608": ""
}

float_suffixes = {
    "32": "q",
    "64": "l"
}

def get_possible_suffixes(memop):
    if memop is None:
        return [""]
    (_, name, _, _, _, w16, w32, w64, is_float) = memop

    if w16 == "AUTO":
        return ["w", "l", "q", ""]
    elif w16 == "VECTOR":
        return ["b", "w", "l", "q", "x", "y", "z", ""]
    else:
        if is_float:
            return list(set([float_suffixes.get(str(w16), ""), float_suffixes.get(str(w32), ""), float_suffixes.get(str(w64), ""), ""]))
        else:
            return list(set([suffixes.get(str(w16), ""), suffixes.get(str(w32), ""), suffixes.get(str(w64), ""), ""]))

def find_mem_for_suffix(operands):
    for operand in operands:
        if operand[1] == "OPERAND_MEM0" or operand[1] == "OPERAND_AGEN":
            return operand
    return None

Id = 0
class Instruction(nonterminals.NonterminalAlternative):
    def __init__(self, props):
        global Id
        super(Instruction, self).__init__("DynamicDecodeInstruction", nonterminals.parse_antecedent(props["PATTERN"]), {})
        assert len(props["PATTERN"]) > 0
        self.pattern = self.antecedent
        self.props = props
        self.disasm = props["DISASM"] if len(props["DISASM"]) > 0 else props["DISASM_ATTSV"]
        self.iclass = props["ICLASS"].strip()
        self.iform = props["IFORM"].strip()
        self.category = props["CATEGORY"].strip()
        self.operands = parse_operands(props["OPERANDS"])
        self.consequent = nonterminals.split_list_by_class([assign for (assign, _, _, _, _, _, _, _, _) in self.operands])
        self.attributes = props["ATTRIBUTES"].split()
        self.uname = props["UNAME"].strip()
        self.has_vexvalid = any([eq.name == "VEXVALID" for eq in self.pattern.get(nonterminals.Equation.__name__, [])])
        self.iname = (self.disasm if len(self.disasm) > 0 else self.iclass).lower()
        self.suffixes = get_possible_suffixes(find_mem_for_suffix(self.operands))
        Id += 1
        self.id = Id

    def __copy__(self):
        return Instruction(self.props)

instruction_properties = ["ATTRIBUTES", "CATEGORY", "DISASM", "DISASM_ATTSV", "ICLASS", "UNAME", "VERSION"] # There are more, but these are the ones we care about. 
repeatable_properties = ["PATTERN", "OPERANDS", "IFORM"] # Some instructions have variants that share the above patterns.
prop = re.compile("^(\\w+)\\s*:([^#]*)(#.*)?$")
def parse_instruction(lines):
    props = {}
    variants = []
    last_pattern = ""
    last_operands = ""
    last_iform = ""
    for name in instruction_properties:
        props[name] = ""
    for line in lines:
        match = prop.match(line)
        if match is not None:
            name = match.group(1).strip()
            result = match.group(2).strip()
            if name in instruction_properties:
                props[name] = result.strip()
            elif name in repeatable_properties:
                if name == "PATTERN": # Starts a new variant
                    if last_pattern != "":
                        variants.append({"PATTERN": last_pattern, "OPERANDS": last_operands, "IFORM": last_iform})
                        last_operands = ""
                        last_iform = ""
                    last_pattern = result
                elif name == "OPERANDS":
                    last_operands = result
                elif name == "IFORM":
                    last_iform = result
    assert len(last_pattern) > 0
    variants.append({"PATTERN": last_pattern, "OPERANDS": last_operands, "IFORM": last_iform}) 
    ret = []
    for var_dict in variants:
        var_dict.update(props)
        ret.append(Instruction(var_dict))
    return ret

def flatten(lst):
    return [elem for sub in lst for elem in sub]

partial_opcode_pattern = re.compile("0b([01_]+)")
def expand_partial_opcodes(instructions):
    new_instructions = []
    for inst in instructions:
        pattern = inst.pattern
        opcodes = pattern[nonterminals.NoncapturingMatch.__name__]
        assert len(opcodes) > 0 and len(opcodes) < 4
        match = partial_opcode_pattern.match(opcodes[-1].pattern)
        if match is None:
            new_instructions.append(inst)
        else:
            bit_sequence = match.group(1).replace("_", "")
            bit_value = int(bit_sequence, 2)
            assert len(bit_sequence) < 8
            num_bits_left = 8 - len(bit_sequence)
            fixed_opcode = (bit_value << num_bits_left)
            for complete_opcode in range(fixed_opcode, fixed_opcode + 2**num_bits_left):
                complete_opcode_string = str(hex(complete_opcode))
                new_inst = copy(inst)
                new_inst.pattern[nonterminals.NoncapturingMatch.__name__][-1] = nonterminals.NoncapturingMatch(complete_opcode_string)
                new_instructions.append(new_inst)
    return new_instructions  


udelete_pattern = re.compile("UDELETE\\s*:\\s*(\\w+)")
def get_deletes(lines):
    deletes = set()
    for line in lines:
        match = udelete_pattern.match(line)
        if match is not None:
            deletes.add(match.groups(1)[0])
    return deletes

def remove_deletes(insts, deletes):
    ret = []
    for inst in insts:
        if inst.uname not in deletes:
            ret.append(inst)
    return ret

instruction_list = None
with open("datafiles/all-dec-instructions.txt", "r") as f:
    lines = f.readlines()
    instruction_list = remove_deletes(expand_partial_opcodes(flatten([parse_instruction(lines) for lines in split_instructions(merge_escaped_newlines(lines))])), get_deletes(lines))
