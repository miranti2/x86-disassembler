class kRewrite(object):
    def __init__(self):
        self._rewrites = []
        self._matches = []
        self._premade = []
        self._attributes = []
        self._condition = None

    def add_variable(self, new_match_cell, new_match_var, new_match_var_sort = ""): # Adds a cell of the form <CELL_NAME> VAR(?:SORT) </CELL_NAME>
        self._matches.append((new_match_cell, new_match_var, new_match_var_sort))
   
    def add_literal_match(self, new_match_cell, new_match_value):
        self._matches.append((new_match_cell, new_match_value, ""))

    def add_premade(self, cell, internal):
        self._premade.append((cell, internal))

    def add_rewrite(self, cell_name, lhs, rhs, full=True):
        if cell_name == "k" and full:
            print("WARNING: Full k rewrite")
        self._rewrites.append((cell_name, lhs, rhs, full))

    def add_attribute(self, attrib):
        self._attributes.append(attrib)

    def set_condition(self, condition):
        self._condition = condition

    def generate_rule(self):
        ret = "rule "
        for (cell_name, lhs, rhs, full) in self._rewrites:
            ret += "<{0}> {1} => {2} {3} </{0}>\n".format(cell_name, lhs, rhs, "" if full else "...")
        for (new_match_cell, new_match_var, new_match_var_sort) in self._matches:
            ret += "<{0}> {1}{2} </{0}>\n".format(new_match_cell, new_match_var, (":" + new_match_var_sort if len(new_match_var_sort) > 0 else ""))
        for ((cell, internal)) in self._premade:
            ret += "<{0}> {1} </{0}>".format(cell, internal)
        if self._condition is not None:
            assert len(self._condition) > 0
            ret += "requires {}".format(self._condition)
        if len(self._attributes) > 0:
            ret += "\t\t\t[" + ", ".join(self._attributes) + "]"
        ret += "\n\n"
        return ret

def c_type_to_k_type(_type):
    return _type # Id for now.

class kParameter(object):
    def __init__(self, name, sort):
        self.name = name
        self.sort = sort

class kConstFunction(object): # Conceptually a const C function, but actually a K macro.
    def __init__(self, name, type, val):
        self.name = name
        self.type = type
        self.val = str(val)
        self.declaration = "syntax {0} ::= {1} [macro]" % (self._type, self._name)
        self.rewrite = kRewrite()
        self.rewrite.add_rewrite("k", self.name, str(val), False)
        self.definition = rewrite.generate_rule()

class kArrayLookupFunction(object):
    def __init__(self, name, type, args, val):
        self.name = name
        self.type = type
        self.val = str(val)
        self.params = params
        self.parameter_type_string = "(" + ','.join([param.sort for param in params]) + ")"

        self.declaration = "syntax {0} ::= {1}{2} [function]" % (self._type, self._name, self._parameter_type_string)
        self.parameter_string =  "(" + ','.join(args) + ")"
        self.rewrite = kRewrite()
#        self.rewrite.add_rewrite(
