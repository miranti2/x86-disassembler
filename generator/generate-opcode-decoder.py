#!/usr/bin/python3
import kcodegen

inum_lookup_table = {}

osz_suffixes = ["b", "w", "l", "q", "x", "y", "z"]

with open("datafiles/inum-opcode-map-groups.csv", "r") as f:
    for line in f.readlines():

        line = line.strip()
        comment_start = line.find("#")
        if comment_start > 0:
            line = line[:(comment_start - 1)]
        elif comment_start == 0:
            continue

        sections = line.split(",")

        assert len(sections) <= 2
        if len(sections) < 2:
            continue # Not a declaration line

        inum = int(sections[0])
        opcode = sections[1]
        
        assert len(opcode) > 0

        existing = inum_lookup_table.get(inum, list())
        existing.append(opcode)
        inum_lookup_table[inum] = existing

rules = []

rule_template = "rule INumOSZToOpcode({0}, {1}) => {2}"

for (inum, opcodes) in inum_lookup_table.items():
    opcode_group = opcodes
    if len(opcodes) > 1:
        first_opcode = opcodes[0]
        has_osz_suffix = first_opcode[-1] in osz_suffixes
        no_suffix = first_opcode[:-1]
        opcode_group = [first_opcode]
        if has_osz_suffix:
            for other_opcode in opcodes[1:]:
                if other_opcode[-1] in osz_suffixes and no_suffix == other_opcode[:-1]:
                    opcode_group.append(other_opcode)
        if len(opcode_group) > 1:
            opcode_group.sort(key=lambda x: osz_suffixes.index(x[-1]))
        assert len(opcode_group) <= 3, "Too many opcodes: {0}".format(','.join(opcode_group))

    if len(opcode_group) == 1:
        rules.append(rule_template.format(inum, "_", opcode_group[0]))
    else:
        for opcode in opcode_group:
            suffix = opcode[-1]
            assert suffix in osz_suffixes
            rules.append(rule_template.format(inum, osz_suffixes.index(suffix) - 1, opcode))




print("""require "x86-syntax.k"
module INUM-TO-OPCODE-FUNCTION
    imports X86-SYNTAX
    syntax Opcode ::= INumOSZToOpcode(Int, Int) [function]
{0}
endmodule""".format('\n'.join(['    ' + rule for rule in rules])))

