#!/bin/bash
set -e


CUR_DIR="$(pwd)"

X86_SEMANTICS="$1"
SCRIPTS="$CUR_DIR/$(dirname $0)"
DECODER="$SCRIPTS/../"


SAMPLES_FILE=$(mktemp)
BINARY_FILE=$(mktemp)
K_INPUT_FILE=$(mktemp)

cat <(find /home/andrewmiranti/Github/X86-64-semantics/semantics/systemInstructions/ -name "*.k") \
    <(find /home/andrewmiranti/Github/X86-64-semantics/semantics/memoryInstructions/ -name "*.k") \
    <(find /home/andrewmiranti/Github/X86-64-semantics/semantics/registerInstructions/ -name "*.k") \
    <(find /home/andrewmiranti/Github/X86-64-semantics/semantics/immediateInstructions/ -name "*.k") | \
    sed -E "s/^.*Instructions\/(.*)\.k$/\1/;/label/d;/opcodes/d" | \
    sort | \
    uniq | \
    sed 's/_/ /g' | \
    "$SCRIPTS/rewrite-ops.py" > $SAMPLES_FILE

as -W -o $BINARY_FILE $SAMPLES_FILE
objdump --insn-width 15 -d $BINARY_FILE | tail -n +8 | grep -Po '\s*[A-Fa-f0-9]+:\s*([A-Fa-f0-9]{2} )*' | sed -E 's/\s//g;s/(\w+):(\w+)/Disassemble(0x\1,0x\2);/' > $K_INPUT_FILE

cd $SCRIPTS
./generate.py > $DECODER/generated_decoder_rules.k
cp "$DECODER/config/extractor-true.k" "$DECODER/inum-extractor-configuration.k"
cp "$DECODER/config/inum-to-opcode.k" "$DECODER/inum-to-opcode.k"
cd $DECODER
kompile test-decoder.k --backend ocaml -I "$X86_SEMANTICS/semantics/"
paste -d, <(./run-on-large-program.sh $K_INPUT_FILE) <(grep -Eo '^(rep\w* )?\w+' $SAMPLES_FILE) |\
    sort -n -k 1 -t, |\
    uniq |\
    uniq --group -w4 > "$SCRIPTS/datafiles/inum-opcode-map-groups.csv"
cd $SCRIPTS
./generate-opcode-decoder.py > "$DECODER/inum-to-opcode.k"
cd $DECODER
cp "./config/extractor-false.k" "./inum-extractor-configuration.k"
kompile test-decoder.k --backend ocaml -I "$X86_SEMANTICS/semantics/"

rm "$SAMPLES_FILE" "$BINARY_FILE" "$K_INPUT_FILE"

cd $CUR_DIR
