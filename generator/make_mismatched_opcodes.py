#!/usr/bin/python3
from disasm_to_iclass import *
from used_opcodes import *

troublesome_opcodes = removed_prefixes - all_opcodes # We have definitions for opcodes we can't ever decode.  This concerns me.

print(len(troublesome_opcodes))
print(sorted(troublesome_opcodes))
