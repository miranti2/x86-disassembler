#BEGIN_LEGAL 
# ADAPTED FROM Intel's XED (See original copyright statement below)
# 
# Copyright (c) 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#                         
# END_LEGAL */

import re

field_re = re.compile("(\w+)\s+\w+\s+(\w+)\s+.*")
with open("datafiles/all-fields.txt") as f:
    print("<decoder>")
    print("\t<decoderBuffer> .Ints </decoderBuffer>")
    print("\t<dynamicDecoderBuffer> .Ints </dynamicDecoderBuffer>")
    for line in f.readlines():
        match = field_re.match(line)
        if match is None:
            continue
        name = match.group(1)
        type_ = match.group(2)
        if type_ == "xed_reg_enum_t":
            print("\t<{0}> REG_INVALID </{0}>".format(name))
        else:
            print("\t<{0}> 0 </{0}>".format(name))
    print("</decoder>")
