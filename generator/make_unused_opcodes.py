#!/usr/bin/python3
from disasm_to_iclass import lookup_table
from used_opcodes import used_opcodes

unused_opcodes = all_opcodes - removed_prefixes
print("syntax UnusedOpcodes ::= {0}".format(" | ".join(["\"{0}\"".format(opcode) for opcode in unused_opcodes])))
