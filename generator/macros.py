import re

macro_pattern = re.compile("\\s*(\\w+)((\\s+\\w+[!]?=\\w+)+)[#]?.*", re.ASCII)

def extract_macro(line):
    match = macro_pattern.match(line)
    if match is None:
        return None
    (macro, target_str, _) = match.groups()
    return (macro, target_str.split())
    

def parse_macros():
    with open("./datafiles/all-state.txt", "r") as f:
        return dict([macro for macro in [extract_macro(line) for line in f.readlines()] if macro is not None])

macro_map = parse_macros()
