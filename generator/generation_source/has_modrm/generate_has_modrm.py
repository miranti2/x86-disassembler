import string

with open('xed-ild-modrm.h', 'r') as f:
    mapnum = 0
    count = 0
    line = f.readline();
    while line:
        if string.find(line, "INC") > -1:
            mapnum = 1
            count = 0
        else:
            line = string.lower(line)
            val = -1
            if string.find(line, "false") > -1:
                val = 0
            elif string.find(line, "true") > -1:
                val = 1
            elif string.find(line, "ignore") > -1:
                val = 2
            elif string.find(line, "undef") > -1:
                val = 3
            print("rule HasMODRM({0}, {1}) => {2}".format(mapnum, count, val))
            count = count + 1
        line = f.readline()

