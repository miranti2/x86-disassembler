#!/usr/bin/python

import string

# Adapted from xed-ild.c

has_disp_regular = [[[0 for i in range(8)] for j in range(4)] for k in range(3)]

has_disp_regular[0][0][6] = 2
for rm in range(8):
    for mod in range(1, 3):
        has_disp_regular[0][mod][rm] = mod

for eamode in range(1, 3):
    for rm in range(8): 
        has_disp_regular[eamode][1][rm] = 1
        has_disp_regular[eamode][2][rm] = 4
    has_disp_regular[eamode][0][5] = 4

for eamode in range(3):
    for mod in range(4):
        for rm in range(8):
            print("rule ComputeRegularDisp({0}, {1}, {2}) => {3}".format(eamode, mod, rm, has_disp_regular[eamode][mod][rm]))
