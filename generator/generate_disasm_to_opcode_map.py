#!/usr/bin/python3

# 
# A Re-Implementation of Intels disassembler from XED in K semantics.
#

import instructions

suffixes = ["b", "w", "l", "q", "x", "y", "z", ""]

print("syntax Opcode ::= DisasmToOpcode(String, String) [function]")

for inst in instructions.instruction_list:
    for suffix in suffixes:
        print("rule DisasmToOpcode(\"{0}\", \"{1}\") => {2}".format(inst.iname, suffix, inst.iname + suffix))
