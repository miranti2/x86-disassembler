#!/bin/bash
CURR=$(pwd)
cd "$(dirname $0)/generator"
./generate.py $(< ./disasm_to_iclass.py $@) > ../generated_decoder_rules.k
cd ..
kompile decoder.k --backend ocaml -I $X86_SEMANTICS
cd $CURR
