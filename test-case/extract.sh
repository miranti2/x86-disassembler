grep "XDIS" elf_reader_xed_out.txt | sed -E 's/XDIS ([a-f0-9]+): \w+\s+\w+\s+([0-9A-F]+).*/Disassemble(0x\1, 0x\2);/' > elf_reader_extracted.txt
grep "XDIS" elf_reader_xed_out.txt | sed -E 's/\s+/ /g' | cut -d' ' -f 6- | sed 's/<.*>//g' > reference-decode.txt
split -a 3 -d file
