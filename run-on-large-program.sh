#!/bin/bash 
#set -x

CUR_DIR="$(pwd)"
SEMANTICS_DIR="$(dirname "$CUR_DIR/$0")"

WORK_DIR="$(mktemp -d)"
OUT_FILE="$(mktemp)"
TEMP_OUT="$(mktemp)"

trap "{ cat \"$OUT_FILE\"; rm -rf \"$WORK_DIR\"; rm -f \"$OUT_FILE\"; rm -f \"$TEMP_OUT\"; cd \"$CUR_DIR\"; }" EXIT 
# https://stackoverflow.com/questions/687014/removing-created-temp-files-in-unexpected-bash-exit

IN_FILE="$(realpath $1)"

cd $WORK_DIR
split -a 3 -d "$IN_FILE"

cd $SEMANTICS_DIR

for filename in $WORK_DIR/*; do # https://stackoverflow.com/questions/20796200/how-to-iterate-over-files-in-a-directory-with-bash
    krun "$filename" > $TEMP_OUT
    LINES_NEEDED="$(wc -l <$filename)"
    LINES_GOT="$(wc -l <$TEMP_OUT)"
    cat "$TEMP_OUT" >> "$OUT_FILE"
    if [ "$LINES_NEEDED" -ne "$LINES_GOT" ] 
        then
        >&2 echo "Lines for $FILE did not match.  Needed $LINES_NEEDED, but got $LINES_GOT"
    fi
done

